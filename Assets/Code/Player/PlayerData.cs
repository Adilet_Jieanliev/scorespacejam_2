using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Create PlayerData", fileName = "Player_0")]
public class PlayerData : ScriptableObject
{
    [Header("Specifications")]
    public int weaponSlots;
    public int liftForce;
    public int currentHealth;
    public int maxHealth;

    /*public Dictionary<�artridgeType, int> magazines = new Dictionary<�artridgeType, int>();*/


    [Header("Weapon")]
    public List<WeaponData> currentWeapon = new List<WeaponData>();

    public PlayerData(PlayerData data) {
        this.weaponSlots = data.weaponSlots;
        this.liftForce = data.liftForce;
        this.currentHealth = data.currentHealth;
        this.maxHealth = data.maxHealth;
        /*this.magazines = new Dictionary<�artridgeType, int>( data.magazines);*/
        this.currentWeapon = new List<WeaponData>(data.currentWeapon);
    }
    /*[Header("Links")]*/
}
