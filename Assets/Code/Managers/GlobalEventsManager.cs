using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public enum Events {
    StartEncounter,
}
public class GlobalEventsManager : MonoBehaviour
{
    public static Dictionary<Events, UnityEvent> events = new Dictionary<Events, UnityEvent>();

    private void Awake() {
        Initialize();
    }
    private void Initialize() {
        events.Add(Events.StartEncounter, new UnityEvent());
    }
}
