using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public PlayerData playerData;

    public ReactiveField<PlayerData> mainPlayerData = new ReactiveField<PlayerData>(null);

    private void Awake() {
        instance = this;
        mainPlayerData.value = new PlayerData(playerData);
    }

    public void StartDelayDestroy(float delay, GameObject obj) {
        StartCoroutine(DelayDestroy(delay, obj));
    }
    private IEnumerator DelayDestroy(float delay, GameObject obj) {
        yield return new WaitForSeconds(delay);
        Destroy(obj);
    }
}

