using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameplayInput : MonoBehaviour
{
    [Header("Settings")]
    public float shootSensetive = 0.99f;

    public static GameplayInput Instance;

    //�� ��� ����������� ����� �����������
    public static ReactiveField<Vector2> movement = new ReactiveField<Vector2>(Vector2.zero);
    public static ReactiveField<Vector2> rotation = new ReactiveField<Vector2>(Vector2.zero);

    public static ReactiveField<bool> shoot = new ReactiveField<bool>(false);
    public static ReactiveField<bool> roll = new ReactiveField<bool>(false);

    private void Awake() {
        if (Instance != null)
            Debug.LogError("��������� ����� 1��� ��");
        Instance = this;

        rotation.action += (Vector2 axies) => {
            if (axies.magnitude > shootSensetive)
                shoot.value = true;
            else
                shoot.value = false;
        };
    }

    public void Update() {
        /*movement.value = new Vector2(leftJoy.Horizontal, leftJoy.Vertical);
        rotation.value = new Vector2(rightJoy.Horizontal, rightJoy.Vertical);*/
    }

    public void RollOn(bool condition) {
        roll.value = condition;
    }
}


