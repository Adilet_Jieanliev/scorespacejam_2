using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateGrid : MonoBehaviour
{
    [Header("Grid")]
    public CellLine[] grid;

    [Header("Settings")]
    public int widzh = 4;
    public int height = 6;
    public Vector2 cellVector = Vector2.one;

    [Header("Links")]
    [SerializeField] private Transform gridparent;
    [SerializeField] private GameObject cellPrefab;

    private void Awake() {
        Create(widzh, height);
    }
    private void Init() {

    }
    private void Create(int widzh, int height) {
        ClearGrid();

        grid = new CellLine[widzh];

        Vector2 startPos;
        startPos.y = -((cellVector.y * ((float)height / 2))) + (cellVector.y / 2);
        startPos.x = -((cellVector.x * ((float)widzh / 2))) + (cellVector.x / 2);

        Vector2 currentPos = startPos;

        for (int w = 0; w < widzh; w++) {
            grid[w] = new CellLine();
            grid[w].line = new Cell[(height)];

            currentPos.y = startPos.y;

            for (int h = 0; h < height; h++) {
                Cell cell = CreateCell(cellPrefab, this.gridparent, currentPos);
                cell.listIndex = (w, h);
                grid[w].line[h] = cell;

                currentPos.y += cellVector.y;
            }
            currentPos.x += cellVector.x;
        }
    }
    private Cell CreateCell(GameObject obj, Transform parent, Vector2 pos) {
        GameObject a = Instantiate(obj, parent);
        a.transform.position = pos;

        return a.GetComponentInParent<Cell>();
    }
    private void ClearGrid() {
        for (int w = 0; w < grid.Length; w++) {
            for (int h = 0; h < grid[h].line.Length; h++) {
                Destroy(grid[w].line[h].gameObject);
            }
        }
    }

    private void Update() {
        if (Input.GetKeyDown(KeyCode.C)) {
            Create(widzh, height);
        }
    }
}
[System.Serializable]
public class CellLine {
    public Cell[] line;
}
