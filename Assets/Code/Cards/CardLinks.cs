using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CardLinks : MonoBehaviour
{
    [Header("Images")]
    public Image mainFrame;
    public Image photo;
    public Image energyFrame;
    [Header("Texts")]
    public TextMeshProUGUI nameText;
    public TextMeshProUGUI energyText;
    public TextMeshProUGUI descriptionText;
}
