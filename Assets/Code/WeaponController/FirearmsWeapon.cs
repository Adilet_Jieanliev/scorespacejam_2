using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.SocialPlatforms.Impl;

public class FirearmsWeapon : Weapon
{
    [SerializeField] private GameObject Muzzle;
    [SerializeField] private Transform muzzleParent;
    [SerializeField] private SpriteRenderer weapontIm;

    private List<Shoot> doulos = new List<Shoot>();

    private void Awake() {
        GameplayInput.rotation.action += HandleShoot;
    }
    private void Start() {
        SetArt();
        CreateMuzzle();
    }
    #region pre-settings
    private void SetArt() {
        weapontIm.sprite = Data.weaponSprite;
    }
    private void CreateMuzzle() {
        float firstZ = -HalfValueCalc(Data.rowDeltaAngle, Data.rowAmount);
        for (int i = 0; i < Data.rowAmount; i++) {
            GameObject a = Instantiate<GameObject>(Muzzle, muzzleParent);
            doulos.Add(a.GetComponent<Shoot>());

            a.transform.localPosition = Vector3.zero;
            a.transform.rotation = Quaternion.Euler(0, 0, firstZ);
            firstZ += Data.rowDeltaAngle;
        }
    }

    private float HalfValueCalc(float delta, int amount) {
        if (amount % 2 == 0)
            return ((amount / 2) * delta) - (delta / 2);
        else
            return ((amount - 1) / 2) * delta;
    }
    #endregion

    #region Shoot 
    bool isFull = false;
    private void Update() {
        
    }
    public void HandleShoot(Vector2 axies) {
        if (axies.magnitude > 0.98) {
            if (Data.hold)
                ShootRate();
            else
                isFull = true;
        }

        if (isFull && axies.magnitude == 0) {
            ShootRate();
            isFull = false;
        }
    }
    float startTime = 0;
    public void ShootRate() {
        //���� ��������
        if (startTime <= Time.time) {
            //������� ���������
            Shoot();
            startTime = Time.time + Data.firingRate;
        }
    }

    public override void Shoot() {
        for (int i = 0; i < doulos.Count; i++) {
            doulos[i].Shoot(Data.bulletForce, Data.bulletLifeTime, Data.bullet);
            //PlayAnimation();
        }
    }

    private void PlayAnimation() {
        LeanTween.cancel(weapontIm.gameObject);
        LeanTween.moveLocal(weapontIm.gameObject,
            new Vector2(weapontIm.transform.position.x - 0.3f,
            weapontIm.transform.position.y ), 0.05f).setLoopPingPong(1);
    }
    #endregion
}
