using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MuzzleController : MonoBehaviour, Shoot
{
    public GameObject bullet;
    public void Shoot(float force, float lifetime, GameObject obj = null) {
        BulletController a;
        if (obj != null)
            a = Instantiate(obj, null).GetComponent<BulletController>();
        else
            a = Instantiate(bullet, null).GetComponent<BulletController>();

        a.transform.position = transform.position;

        a.rb.AddForce(transform.right * force, ForceMode2D.Impulse);

        GameManager.instance.StartDelayDestroy(lifetime, a.gameObject);
    }
    
}
