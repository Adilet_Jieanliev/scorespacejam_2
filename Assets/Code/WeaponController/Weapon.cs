using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [HideInInspector] private WeaponData data;

    public WeaponData Data {
        get { return data; }
        set {
            data = value;
        }
    }
    public virtual void Shoot() {
        
    }

}
